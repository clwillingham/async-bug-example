const read = require('../utils/read');
const extract = require('../extractors/CSVFile');

// const postal = require('@cymen/node-postal');

function range(size, startAt = 0) {
    return [...Array(size).keys()].map(i => i + startAt);
}

function characterRange(startChar, endChar) {
    return String.fromCharCode(...range(endChar.charCodeAt(0) -
        startChar.charCodeAt(0), startChar.charCodeAt(0)), endChar.charCodeAt(0))
}

module.exports.action = async function(dataset, {Household, State}, {sequelize}) {

}